
window.onload = function() {
  var TimeElArr = [];
  var TimeVarArr = [];
  var newArr = [];
  var interval;
  var style = 'width: 100px; background-color: #1a2233; height: 30px; margin-right: 10px; margin-left: 10px; border: 0px; border-radius: 5px; color: white;'
  var minlabel = "First date";
  var maxlabel;
  /* Function to show relative time in text*/ 
  function RelativeTime(){
    clearInterval(interval);
    interval = setInterval(function(){
      for(var i = 0; document.getElementsByTagName("time")[i] != null; i++)
      { 
        TimeElArr.push(document.getElementsByTagName("time")[i]);
        TimeVarArr.push(document.getElementsByTagName("time")[i].innerHTML);
        var a = new Date(TimeElArr[i].getAttribute("datetime"));
        TimeElArr[i].innerHTML = GetDifrence(a);
      }
    }, 1000);
  }

  /** Function to returt text content of time*/
  function TextTime(){
    if(TimeVarArr[0] != null){
      for(var i = 0; document.getElementsByTagName("time")[i] != null; i++)
      { 
        var a = TimeVarArr[i];
        TimeElArr[i].innerHTML = a;
      }
    }
  }

  /** Function to show time in time format */
  function NormalTime(){
    if(TimeElArr[0] != null){
      for(var i = 0; document.getElementsByTagName("time")[i] != null; i++)
        {
          var a = TimeElArr[i].getAttribute("datetime");
          TimeElArr[i].innerHTML = a;
        }
    } else {
      for(var i = 0; document.getElementsByTagName("time")[i] != null; i++)
      { 
        TimeElArr.push(document.getElementsByTagName("time")[i]);
        TimeVarArr.push(document.getElementsByTagName("time")[i].innerHTML);
        var a = TimeElArr[i].getAttribute("datetime");
        TimeElArr[i].innerHTML = a;
      }
    }
  }

  //Main Div
  var TimeDiv = document.getElementsByClassName("controls")[0];
  TimeDiv.style.cssText = 'width: 90%; height: 180px; border: 5px solid #618685; margin-left: auto; margin-right: auto; border-radius: 15px; padding: 10px; margin-bottom: 10px;';

  //Button Div
  var ButtDiv = document.createElement("Div");
  ButtDiv.style.cssText = 'width: 380px; height: 30px; margin-left: auto; margin-right: auto;';

  //Timeline Div
  var TimeLine = document.createElement("Div");
  TimeLine.style.cssText = 'width: 100%; height: 10px; margin-left: auto; margin-right: auto; border-radius: 5px; border: 1px solid #618685; margin-top: 40px; position: relative;';

  /** Add circle on timeline*/
  function addCircle(position, title){
      var circle = document.createElement("DIV");
      circle.style.cssText = 'width: 10px; height: 10px; background-color: #1a2233; border-radius: 5px; border: 0px;  ' + position;
      circle.setAttribute("title", title);
      TimeLine.appendChild(circle);
  }



  var TimeBtn = document.createElement("BUTTON");
  TimeBtn.style.cssText = style;
  var TextTimeBtn = document.createTextNode("Return text");
  TimeBtn.appendChild(TextTimeBtn);

  var NormalTimeBtn = document.createElement("BUTTON");
  NormalTimeBtn.style.cssText = style;
  var NormalTextTimeBtn = document.createTextNode("Normal time");
  NormalTimeBtn.appendChild(NormalTextTimeBtn);

  var RelTimeBtn = document.createElement("BUTTON");
  RelTimeBtn.style.cssText = style;
  var RelTextTimeBtn = document.createTextNode("Relative time");
  RelTimeBtn.appendChild(RelTextTimeBtn);

  /** Label of firt date title */
  var MinLabel = document.createElement("LABEL");
  MinLabel.style.cssText = "float: left; border-radius: 5px; border: 1px solid #618685; width: 80px; margin-top: 10px;";

  /** Label of firt date title */
  var MaxLabel = document.createElement("LABEL");
  MaxLabel.style.cssText = "float: right; border-radius: 5px; border: 1px solid #618685; width: 80px; margin-top: 10px;";

  /**Add elements to div */
  ButtDiv.appendChild(TimeBtn);
  ButtDiv.appendChild(NormalTimeBtn);
  ButtDiv.appendChild(RelTimeBtn);
  ToTimeLine();

  //addCircle('float: left;', 'Some');
  TimeDiv.appendChild(ButtDiv);
  TimeDiv.appendChild(TimeLine);
  TimeDiv.appendChild(MinLabel);
  TimeDiv.appendChild(MaxLabel);

  TimeBtn.onclick = function(){
    clearInterval(interval);
    TextTime();
  };

  RelTimeBtn.onclick = function(){
    RelativeTime();
  };

  NormalTimeBtn.onclick = function(){
    clearInterval(interval);
    NormalTime();
  };

  /** Function to add timeline*/

  function ToTimeLine(){
      var i_max = 0;

      for(var i = 0; document.getElementsByTagName("time")[i] != null; i++)
          { 
              newArr.push(document.getElementsByTagName("time")[i]);
              i_max++;
          }

          //var i_min = 0;
          var max = Number.MIN_SAFE_INTEGER;
          var valueMax;
          var valueMin;
          var min = Number.MAX_SAFE_INTEGER;
          
        for(var i = 0; i < i_max; i++)
          { 
            var b = new Date(newArr[i].getAttribute("datetime"));
              if(b.getTime() > max) {
                  max = b.getTime();
                  maxlabel = newArr[i].title;
                  valueMax = newArr[i].getAttribute("datetime") + '\n' +newArr[i].title;
                  min = b.getTime();
                  valueMin = newArr[i].getAttribute("datetime") + '\n' +newArr[i].title;
              }
          }

        for(var i = 0; i<i_max; i++)
          { 
              var b = new Date(newArr[i].getAttribute("datetime"));
              var c = Math.abs(b.getTime());
              if(b.getTime() < min) {
                  min = b.getTime();
                  minlabel = newArr[i].title;
                  valueMin = newArr[i].getAttribute("datetime") + '\n' +newArr[i].title;
              }
          }

        var MinLabelText = document.createTextNode("First:" + '\n' + minlabel);
        MinLabel.appendChild(MinLabelText);
        var MaxLabelText = document.createTextNode("Last:" + '\n' + maxlabel);
        MaxLabel.appendChild(MaxLabelText);

        if(valueMax == valueMin) {
          addCircle('left: 50%; position: absolute;', valueMax);
        } else {
          addCircle('float: left;', valueMin);
          addCircle('float: right;', valueMax);
        }

        for(var i = 0; i < i_max; i++) {
              var b = new Date(newArr[i].getAttribute("datetime"));
              if(newArr[i].getAttribute("datetime") + '\n' +newArr[i].title !== valueMax && newArr[i].getAttribute("datetime") + '\n' +newArr[i].title !== valueMin) {
                var x = Math.round(((b.getTime() - min)*100)/(max - min));
                var value = newArr[i].getAttribute("datetime") + '\n' +newArr[i].title;
                addCircle('left:' + x + '%; position: absolute;', value);
            }
          }
          

  }

  /** Function to get difrence between nowdate and text date */
  function GetDifrence(date){
    var OneDay = 1000*60*60*24;
    var OneHour = 1000*60*60;
    var OneMinute = 1000*60;
    
    
    var date1 = new Date();
    var DifDate = Math.abs(date1.getTime() - date.getTime());
    console.log(new Date(DifDate))
    
    var DayCount = Math.floor(DifDate/OneDay);
    var HourCount = Math.floor((DifDate - DayCount*OneDay)/OneHour);
    var MinuteCount = Math.floor((DifDate - (DayCount*OneDay + HourCount*OneHour))/OneMinute);
    
    if(date1.getFullYear() < date.getFullYear()){
      return String(DayCount) + " days " + String(HourCount) + " hours " + String(MinuteCount) + " minuts left"
    }else{
    return String(DayCount) + " days " + String(HourCount) + " hours " + String(MinuteCount) + " minuts ago";}
    //return MinuteCount;
  }
}


